<?php
// src/Twig/AppRuntime.php
namespace App\Twig;

use Twig\Extension\RuntimeExtensionInterface;

use Doctrine\ORM\EntityManager;

class AppRuntime implements RuntimeExtensionInterface
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function priceFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = $price.' DT';

        return $price;
    }

    public function prixFabrication($bijou)
    {
        $prixFabrication = $this->em->getRepository("App:Produit")->getPrixFabrication($bijou);

        return $prixFabrication;
    }

    public function qteFabricable($bijou)
    {
        $qteFabricable = $this->em->getRepository("App:Produit")->getQteFabricable($bijou);

        return $qteFabricable;
    }

    public function etatFabricationBadgeFilter($string)
    {
        if($string == "Fabriqué"){
            return '<span class="badge badge-success">'.$string.'</span>';
        }else
            return '<span class="badge badge-warning">'.$string.'</span>';
    }

    
}