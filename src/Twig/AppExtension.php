<?php

// src/Twig/AppExtension.php
namespace App\Twig;

use App\Twig\AppRuntime;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            // the logic of this filter is now implemented in a different class
            new TwigFilter('price', [AppRuntime::class, 'priceFilter']),
            new TwigFilter('etatFabricationBadge', [AppRuntime::class, 'etatFabricationBadgeFilter']),
        ];
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('prixFabrication', [AppRuntime::class, 'prixFabrication']),
            new TwigFunction('qteFabricable', [AppRuntime::class, 'qteFabricable']),
        ];
    }
}