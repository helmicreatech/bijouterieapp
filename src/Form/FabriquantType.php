<?php

namespace App\Form;

use App\Entity\Fabriquant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FabriquantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('prenom')
            ->add('civilite')
            ->add('entreprise')
            //->add('cin')
            ->add('telephone')
            ->add('telephone2')
            ->add('fax')
            ->add('email')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Fabriquant::class,
        ]);
    }
}
