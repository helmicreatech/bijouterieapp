<?php

namespace App\Form;

use App\Entity\LigneDevis;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use App\Entity\Produit;



class LigneDevisType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quantite')
            ->add('prixUnitaire')
            ->add('bijou', EntityType::class, [
            // looks for choices from this entity
            'class' => Produit::class,

            // uses the User.username property as the visible option string
            'choice_label' => 'reference',
            'choice_value' => 'prixVente',
            'required' => false,
            'group_by' => 'categorie'

            // used to render a select box, check boxes or radios
            // 'multiple' => true,
            // 'expanded' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LigneDevis::class,
        ]);
    }
}
