<?php

namespace App\Form;

use App\Entity\TypePierre;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class TypePierreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libele')
            ->add('unite', ChoiceType::class, [
                                                'choices'  => [
                                                    'Carat' => 'ct',
                                                    'Gramme' => 'g',
                                                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TypePierre::class,
        ]);
    }
}
