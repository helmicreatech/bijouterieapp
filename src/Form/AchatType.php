<?php

namespace App\Form;

use App\Entity\Achat;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use App\Entity\Produit;
use App\Entity\Fournisseur;

class AchatType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('prix')
        ->add('date', DateType::class, [
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
        ])
            ->add('quantite')
            ->add('produit', EntityType::class, [
            // looks for choices from this entity
            'class' => Produit::class,
            'choice_label' => 'reference',
            'group_by' => 'categorie'
        ])
        ->add('fournisseur', EntityType::class, [
            // looks for choices from this entity
            'class' => Fournisseur::class,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Achat::class,
        ]);
    }
}
