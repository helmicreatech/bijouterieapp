<?php

namespace App\Form;

use App\Entity\Produit;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class GoldType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('caratOr', ChoiceType::class, [
                                                'choices'  => [
                                                    '9' => '9',
                                                    '18' => '18',
                                                ],
            ])
            ->add('couleur', ChoiceType::class, [
                                                'choices'  => [
                                                    'Jaune' => 'Jaune',
                                                    'Rose' => 'Rose',
                                                    'Blanc' => 'Blanc',
                                                ],
            ])
            ->add('traitement', ChoiceType::class, [
                                                'choices'  => [
                                                    'A la main' => 'A la main',
                                                    'Sous pression' => 'Sous pression',
                                                ],
            ])
            ->add('reference')
            ->add('prixAchat')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Produit::class,
        ]);
    }
}
