<?php

namespace App\Form;

use App\Entity\Produit;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use App\Entity\TypePierre;
use App\Entity\Carat;

class PierreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('libele')
            ->add('carat', EntityType::class, [
            // looks for choices from this entity
                'class' => Carat::class,
            ])
            ->add('typePierre', EntityType::class, [
            // looks for choices from this entity
                'class' => TypePierre::class,
            ])
            ->add('prixAchat')
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Produit::class,
        ]);
    }
}
