<?php

namespace App\Form;

use App\Entity\OrdreFabrication;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Doctrine\ORM\EntityRepository;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use App\Entity\Produit;

class OrdreFabricationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quantite')
            ->add('prix')
            ->add('avance')
            ->add('date', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
            ])
            ->add('dateLivraison', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'required' => false
            ])
            ->add('etat', ChoiceType::class, [
                                                'choices'  => [
                                                    'En cours de fabrication' => 'En cours de fabrication',
                                                    'Fabriqué' => 'Fabriqué',
                                                ],
            ])
            ->add('etatPaiement', ChoiceType::class, [
                                                'choices'  => [
                                                    'Payé' => 'Payé',
                                                    'Non payé' => 'Non Payé',
                                                ],
            ])
            ->add('utiliserStockInterne', ChoiceType::class, [
                                                'choices'  => [
                                                    'Oui' => 'Oui',
                                                    'Non' => 'Non',
                                                ],
            ])
            ->add('bijou', EntityType::class, [
                // looks for choices from this entity
                'class' => Produit::class,
                'choice_label' => 'reference',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->andWhere('p.categorie = :val')
                        ->setParameter('val', 3);
                },
            ])
            ->add('fabriquant')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrdreFabrication::class,
        ]);
    }
}
