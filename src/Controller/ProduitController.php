<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Produit;
use App\Entity\ImageProduit;
use App\Repository\ProduitRepository;
use App\Repository\CategorieRepository;

use App\Form\ProduitType;
use App\Form\BijouType;
use App\Form\GoldType;
use App\Form\PierreType;
use App\Form\AccessoireType;


/**
 * @Route("/produit")
 */
class ProduitController extends AbstractController
{
    /**
     * @Route("/", name="produit_index", methods={"GET"})
     */
    public function index(ProduitRepository $produitRepository): Response
    {
        return $this->render('produit/index.html.twig', [
            'produits' => $produitRepository->findAll(),
        ]);
    }

    /**
     * @Route("/gold", name="produit_gold_index", methods={"GET"})
     */
    public function getAllGolds(ProduitRepository $produitRepository): Response
    {
        return $this->render('gold/index.html.twig', [
            'golds' => $produitRepository->findAllGolds(),
        ]);
    }

    /**
     * @Route("/pierres", name="produit_pierre_index", methods={"GET"})
     */
    public function getAllPierres(ProduitRepository $produitRepository): Response
    {
        return $this->render('pierre/index.html.twig', [
            'pierres' => $produitRepository->findAllPierres(),
        ]);
    }

    /**
     * @Route("/bijoux", name="produit_bijou_index", methods={"GET"})
     */
    public function getAllBijoux(ProduitRepository $produitRepository): Response
    {
        return $this->render('bijou/index.html.twig', [
            'bijous' => $produitRepository->findAllBijoux(),
        ]);
    }

    /**
     * @Route("/accessoires", name="produit_accessoire_index", methods={"GET"})
     */
    public function getAllAccessoires(ProduitRepository $produitRepository): Response
    {
        return $this->render('accessoire/index.html.twig', [
            'accessoires' => $produitRepository->findAllAccessoires(),
        ]);
    }

    /**
     * @Route("/new", name="produit_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $produit = new Produit();
        $form = $this->createForm(ProduitType::class, $produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($produit);
            $entityManager->flush();

            return $this->redirectToRoute('produit_index');
        }

        return $this->render('produit/new.html.twig', [
            'produit' => $produit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/gold/new", name="produit_gold_new", methods={"GET","POST"})
     */
    public function newOr(Request $request, CategorieRepository $categorieRepository): Response
    {
        $produit = new Produit();
        $form = $this->createForm(GoldType::class, $produit);
        $form->handleRequest($request);

        $categorie = $categorieRepository->find(1);

        $produit->setCategorie($categorie);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
             $image = $this->saveFile($request, 'image', $produit, new ImageProduit(), $this->getDoctrine()->getManager(), true);
            $entityManager->persist($produit);
            $entityManager->flush();

            return $this->redirectToRoute('produit_gold_index');
        }

        return $this->render('gold/new.html.twig', [
            'produit' => $produit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/pierre/new", name="produit_pierre_new", methods={"GET","POST"})
     */
    public function newPierre(Request $request, CategorieRepository $categorieRepository): Response
    {
        $produit = new Produit();
        $form = $this->createForm(PierreType::class, $produit);
        $form->handleRequest($request);

        $categorie = $categorieRepository->find(2);

        $produit->setCategorie($categorie);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
             $image = $this->saveFile($request, 'image', $produit, new ImageProduit(), $this->getDoctrine()->getManager(), true);
            $entityManager->persist($produit);
            $entityManager->flush();

            return $this->redirectToRoute('produit_pierre_index');
        }

        return $this->render('pierre/new.html.twig', [
            'produit' => $produit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/bijou/new", name="produit_bijou_new", methods={"GET","POST"})
     */
    public function newBijou(Request $request, CategorieRepository $categorieRepository): Response
    {
        $produit = new Produit();
        $form = $this->createForm(BijouType::class, $produit);
        $form->handleRequest($request);

        $categorie = $categorieRepository->find(3);

        $produit->setCategorie($categorie);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
             $image = $this->saveFile($request, 'image', $produit, new ImageProduit(), $this->getDoctrine()->getManager(), true);
            $entityManager->persist($produit);
            $entityManager->flush();

            return $this->redirectToRoute('produit_bijou_index');
        }

        return $this->render('bijou/new.html.twig', [
            'produit' => $produit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/accessoire/new", name="produit_accessoire_new", methods={"GET","POST"})
     */
    public function newAccessoire(Request $request, CategorieRepository $categorieRepository): Response
    {
        $produit = new Produit();
        $form = $this->createForm(AccessoireType::class, $produit);
        $form->handleRequest($request);

        $categorie = $categorieRepository->find(4);

        $produit->setCategorie($categorie);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
             $image = $this->saveFile($request, 'image', $produit, new ImageProduit(), $this->getDoctrine()->getManager(), true);
            $entityManager->persist($produit);
            $entityManager->flush();

            return $this->redirectToRoute('produit_accessoire_index');
        }

        return $this->render('accessoire/new.html.twig', [
            'produit' => $produit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="produit_show", methods={"GET"})
     */
    public function show(Produit $produit): Response
    {
        return $this->render('produit/show.html.twig', [
            'produit' => $produit,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="produit_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Produit $produit): Response
    {
        $form = $this->createForm(ProduitType::class, $produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('produit_index', [
                'id' => $produit->getId(),
            ]);
        }

        return $this->render('produit/edit.html.twig', [
            'produit' => $produit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/gold/{id}/edit", name="produit_gold_edit", methods={"GET","POST"})
     */
    public function editGold(Request $request, Produit $produit): Response
    {
        $form = $this->createForm(GoldType::class, $produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!empty($request->files->get('image')))
            {
                if ($produit->getImage())
                {
                    $this->getDoctrine()->getManager()->remove($produit->getImage());
                    $this->getDoctrine()->getManager()->flush();
                }
                $image = $this->saveFile($request, 'image', $produit, new ImageProduit(), $this->getDoctrine()->getManager(), true);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('produit_gold_index', [
                'id' => $produit->getId(),
            ]);
        }

        return $this->render('gold/edit.html.twig', [
            'produit' => $produit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/pierre/{id}/edit", name="produit_pierre_edit", methods={"GET","POST"})
     */
    public function editPierre(Request $request, Produit $produit): Response
    {
        $form = $this->createForm(PierreType::class, $produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!empty($request->files->get('image')))
            {
                if ($produit->getImage())
                {
                    $this->getDoctrine()->getManager()->remove($produit->getImage());
                    $this->getDoctrine()->getManager()->flush();
                }
                $image = $this->saveFile($request, 'image', $produit, new ImageProduit(), $this->getDoctrine()->getManager(), true);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('produit_pierre_index', [
                'id' => $produit->getId(),
            ]);
        }

        return $this->render('pierre/edit.html.twig', [
            'produit' => $produit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/bijou/{id}/edit", name="produit_bijou_edit", methods={"GET","POST"})
     */
    public function editBijou(Request $request, Produit $produit): Response
    {
        $form = $this->createForm(BijouType::class, $produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!empty($request->files->get('image')))
            {
                if ($produit->getImage())
                {
                    $this->getDoctrine()->getManager()->remove($produit->getImage());
                    $this->getDoctrine()->getManager()->flush();
                }
                $image = $this->saveFile($request, 'image', $produit, new ImageProduit(), $this->getDoctrine()->getManager(), true);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('produit_bijou_index', [
                'id' => $produit->getId(),
            ]);
        }

        return $this->render('bijou/edit.html.twig', [
            'produit' => $produit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/accessoire/{id}/edit", name="produit_accessoire_edit", methods={"GET","POST"})
     */
    public function editAccessoire(Request $request, Produit $produit): Response
    {
        $form = $this->createForm(AccessoireType::class, $produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!empty($request->files->get('image')))
            {
                if ($produit->getImage())
                {
                    $this->getDoctrine()->getManager()->remove($produit->getImage());
                    $this->getDoctrine()->getManager()->flush();
                }
                $image = $this->saveFile($request, 'image', $produit, new ImageProduit(), $this->getDoctrine()->getManager(), true);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('produit_accessoire_index', [
                'id' => $produit->getId(),
            ]);
        }

        return $this->render('accessoire/edit.html.twig', [
            'produit' => $produit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="produit_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Produit $produit): Response
    {

        if ($this->isCsrfTokenValid('delete'.$produit->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($produit);
            $entityManager->flush();
        }

        if($produit->getCategorie()->getId()==1)
            return $this->redirectToRoute('produit_gold_index');
        if($produit->getCategorie()->getId()==2)
            return $this->redirectToRoute('produit_pierre_index');
        if($produit->getCategorie()->getId()==3)
            return $this->redirectToRoute('produit_bijou_index');
        if($produit->getCategorie()->getId()==4)
            return $this->redirectToRoute('produit_accessoire_index');

        
    }

    private function saveFile($request, $fileName, $entity, $joinEntity, $em, $flush = false)
    {
        $uploadedFile = $request->files->get($fileName);
        $setFunctionName = "set" . ucfirst($fileName);

        if (!empty($uploadedFile))
        {
            $file = $joinEntity;
            $file->setFile($uploadedFile);
            // Pass additional data
            $additionalData = new \stdClass();
            $additionalData->kernel = null;
            $additionalData->request = $request;
            $file->setAdditionalData($additionalData);

            $entity->$setFunctionName($file);
            $em->persist($entity);
            if ($flush)
                $em->flush();
            return $file;
        }
    }
}
