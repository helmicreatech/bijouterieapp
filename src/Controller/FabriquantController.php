<?php

namespace App\Controller;

use App\Entity\Fabriquant;
use App\Form\FabriquantType;
use App\Repository\FabriquantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/fabriquant")
 */
class FabriquantController extends AbstractController
{
    /**
     * @Route("/", name="fabriquant_index", methods={"GET"})
     */
    public function index(FabriquantRepository $fabriquantRepository): Response
    {
        return $this->render('fabriquant/index.html.twig', [
            'fabriquants' => $fabriquantRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="fabriquant_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $fabriquant = new Fabriquant();
        $form = $this->createForm(FabriquantType::class, $fabriquant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($fabriquant);
            $entityManager->flush();

            return $this->redirectToRoute('fabriquant_index');
        }

        return $this->render('fabriquant/new.html.twig', [
            'fabriquant' => $fabriquant,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="fabriquant_show", methods={"GET"})
     */
    public function show(Fabriquant $fabriquant): Response
    {
        return $this->render('fabriquant/show.html.twig', [
            'fabriquant' => $fabriquant,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="fabriquant_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Fabriquant $fabriquant): Response
    {
        $form = $this->createForm(FabriquantType::class, $fabriquant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('fabriquant_index', [
                'id' => $fabriquant->getId(),
            ]);
        }

        return $this->render('fabriquant/edit.html.twig', [
            'fabriquant' => $fabriquant,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="fabriquant_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Fabriquant $fabriquant): Response
    {
        if ($this->isCsrfTokenValid('delete'.$fabriquant->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($fabriquant);
            $entityManager->flush();
        }

        return $this->redirectToRoute('fabriquant_index');
    }
}
