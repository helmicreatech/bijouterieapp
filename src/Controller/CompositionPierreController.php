<?php

namespace App\Controller;

use App\Entity\CompositionPierre;
use App\Form\CompositionPierreType;
use App\Repository\CompositionPierreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/composition/pierre")
 */
class CompositionPierreController extends AbstractController
{
    /**
     * @Route("/", name="composition_pierre_index", methods={"GET"})
     */
    public function index(CompositionPierreRepository $compositionPierreRepository): Response
    {
        return $this->render('composition_pierre/index.html.twig', [
            'composition_pierres' => $compositionPierreRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="composition_pierre_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $compositionPierre = new CompositionPierre();
        $form = $this->createForm(CompositionPierreType::class, $compositionPierre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($compositionPierre);
            $entityManager->flush();

            return $this->redirectToRoute('composition_pierre_index');
        }

        return $this->render('composition_pierre/new.html.twig', [
            'composition_pierre' => $compositionPierre,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="composition_pierre_show", methods={"GET"})
     */
    public function show(CompositionPierre $compositionPierre): Response
    {
        return $this->render('composition_pierre/show.html.twig', [
            'composition_pierre' => $compositionPierre,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="composition_pierre_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CompositionPierre $compositionPierre): Response
    {
        $form = $this->createForm(CompositionPierreType::class, $compositionPierre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('composition_pierre_index', [
                'id' => $compositionPierre->getId(),
            ]);
        }

        return $this->render('composition_pierre/edit.html.twig', [
            'composition_pierre' => $compositionPierre,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="composition_pierre_delete", methods={"DELETE"})
     */
    public function delete(Request $request, CompositionPierre $compositionPierre): Response
    {
        if ($this->isCsrfTokenValid('delete'.$compositionPierre->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($compositionPierre);
            $entityManager->flush();
        }

        return $this->redirectToRoute('composition_pierre_index');
    }
}
