<?php

namespace App\Controller;

use App\Entity\CompositionOr;
use App\Form\CompositionOrType;
use App\Repository\CompositionOrRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/composition/or")
 */
class CompositionOrController extends AbstractController
{
    /**
     * @Route("/", name="composition_or_index", methods={"GET"})
     */
    public function index(CompositionOrRepository $compositionOrRepository): Response
    {
        return $this->render('composition_or/index.html.twig', [
            'composition_ors' => $compositionOrRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="composition_or_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $compositionOr = new CompositionOr();
        $form = $this->createForm(CompositionOrType::class, $compositionOr);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($compositionOr);
            $entityManager->flush();

            return $this->redirectToRoute('composition_or_index');
        }

        return $this->render('composition_or/new.html.twig', [
            'composition_or' => $compositionOr,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="composition_or_show", methods={"GET"})
     */
    public function show(CompositionOr $compositionOr): Response
    {
        return $this->render('composition_or/show.html.twig', [
            'composition_or' => $compositionOr,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="composition_or_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CompositionOr $compositionOr): Response
    {
        $form = $this->createForm(CompositionOrType::class, $compositionOr);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('composition_or_index', [
                'id' => $compositionOr->getId(),
            ]);
        }

        return $this->render('composition_or/edit.html.twig', [
            'composition_or' => $compositionOr,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="composition_or_delete", methods={"DELETE"})
     */
    public function delete(Request $request, CompositionOr $compositionOr): Response
    {
        if ($this->isCsrfTokenValid('delete'.$compositionOr->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($compositionOr);
            $entityManager->flush();
        }

        return $this->redirectToRoute('composition_or_index');
    }
}
