<?php

namespace App\Controller;

use App\Entity\LigneDevis;
use App\Form\LigneDevisType;
use App\Repository\LigneDevisRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ligne/devis")
 */
class LigneDevisController extends AbstractController
{
    /**
     * @Route("/", name="ligne_devis_index", methods={"GET"})
     */
    public function index(LigneDevisRepository $ligneDevisRepository): Response
    {
        return $this->render('ligne_devis/index.html.twig', [
            'ligne_devis' => $ligneDevisRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="ligne_devis_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $ligneDevi = new LigneDevis();
        $form = $this->createForm(LigneDevisType::class, $ligneDevi);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ligneDevi);
            $entityManager->flush();

            return $this->redirectToRoute('ligne_devis_index');
        }

        return $this->render('ligne_devis/new.html.twig', [
            'ligne_devi' => $ligneDevi,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="ligne_devis_show", methods={"GET"})
     */
    public function show(LigneDevis $ligneDevi): Response
    {
        return $this->render('ligne_devis/show.html.twig', [
            'ligne_devi' => $ligneDevi,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="ligne_devis_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, LigneDevis $ligneDevi): Response
    {
        $form = $this->createForm(LigneDevisType::class, $ligneDevi);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ligne_devis_index', [
                'id' => $ligneDevi->getId(),
            ]);
        }

        return $this->render('ligne_devis/edit.html.twig', [
            'ligne_devi' => $ligneDevi,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="ligne_devis_delete", methods={"DELETE"})
     */
    public function delete(Request $request, LigneDevis $ligneDevi): Response
    {
        if ($this->isCsrfTokenValid('delete'.$ligneDevi->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ligneDevi);
            $entityManager->flush();
        }

        return $this->redirectToRoute('ligne_devis_index');
    }
}
