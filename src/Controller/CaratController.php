<?php

namespace App\Controller;

use App\Entity\Carat;
use App\Form\CaratType;
use App\Repository\CaratRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/carat")
 */
class CaratController extends AbstractController
{
    /**
     * @Route("/", name="carat_index", methods={"GET"})
     */
    public function index(CaratRepository $caratRepository): Response
    {
        return $this->render('carat/index.html.twig', [
            'carats' => $caratRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="carat_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $carat = new Carat();
        $form = $this->createForm(CaratType::class, $carat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($carat);
            $entityManager->flush();

            return $this->redirectToRoute('carat_index');
        }

        return $this->render('carat/new.html.twig', [
            'carat' => $carat,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="carat_show", methods={"GET"})
     */
    public function show(Carat $carat): Response
    {
        return $this->render('carat/show.html.twig', [
            'carat' => $carat,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="carat_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Carat $carat): Response
    {
        $form = $this->createForm(CaratType::class, $carat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('carat_index', [
                'id' => $carat->getId(),
            ]);
        }

        return $this->render('carat/edit.html.twig', [
            'carat' => $carat,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="carat_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Carat $carat): Response
    {
        if ($this->isCsrfTokenValid('delete'.$carat->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($carat);
            $entityManager->flush();
        }

        return $this->redirectToRoute('carat_index');
    }
}
