<?php

namespace App\Controller;

use App\Entity\TypePierre;
use App\Form\TypePierreType;
use App\Repository\TypePierreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/type/pierre")
 */
class TypePierreController extends AbstractController
{
    /**
     * @Route("/", name="type_pierre_index", methods={"GET"})
     */
    public function index(TypePierreRepository $typePierreRepository): Response
    {
        return $this->render('type_pierre/index.html.twig', [
            'type_pierres' => $typePierreRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="type_pierre_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $typePierre = new TypePierre();
        $form = $this->createForm(TypePierreType::class, $typePierre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($typePierre);
            $entityManager->flush();

            return $this->redirectToRoute('type_pierre_index');
        }

        return $this->render('type_pierre/new.html.twig', [
            'type_pierre' => $typePierre,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_pierre_show", methods={"GET"})
     */
    public function show(TypePierre $typePierre): Response
    {
        return $this->render('type_pierre/show.html.twig', [
            'type_pierre' => $typePierre,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="type_pierre_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TypePierre $typePierre): Response
    {
        $form = $this->createForm(TypePierreType::class, $typePierre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('type_pierre_index', [
                'id' => $typePierre->getId(),
            ]);
        }

        return $this->render('type_pierre/edit.html.twig', [
            'type_pierre' => $typePierre,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_pierre_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TypePierre $typePierre): Response
    {
        if ($this->isCsrfTokenValid('delete'.$typePierre->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($typePierre);
            $entityManager->flush();
        }

        return $this->redirectToRoute('type_pierre_index');
    }
}
