<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


use App\Repository\ProduitRepository;
use App\Repository\FactureRepository;
use App\Repository\OrdreFabricationRepository;

class DashboardController extends AbstractController
{
    /**
     * @Route("/", name="dashboard")
     */
    public function index(ProduitRepository $produitRepository, FactureRepository $factureRepository, OrdreFabricationRepository $ordreFabricationRepository)
    {
        return $this->render('dashboard/index.html.twig', [
            'chiffreAffaireStockOr'       => $produitRepository->getChiffreAffaireOr(),
            'chiffreAffaireStockPierre'         => $produitRepository->getChiffreAffairePierre(),
            'chiffreAffaireStockAccessoires'         => $produitRepository->getChiffreAffaireAccessoires(),
            'getChiffreAffaireBijoux'     => $produitRepository->getChiffreAffaireBijoux(),
            'getChiffreAffaireBijouxEnCoursDeFabrication'  => $produitRepository->getChiffreAffaireBijouxEnCoursDeFabrication(),
            'factures' => $factureRepository->findBy(array(), array('id' => 'DESC'),5),
            'ordres' => $ordreFabricationRepository->findBy(array(), array('id' => 'DESC'),5),
        ]);
    }
}
