<?php

namespace App\Controller;

use App\Entity\OrdreFabrication;
use App\Form\OrdreFabricationType;
use App\Repository\OrdreFabricationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ordre/fabrication")
 */
class OrdreFabricationController extends AbstractController
{
    /**
     * @Route("/", name="ordre_fabrication_index", methods={"GET"})
     */
    public function index(OrdreFabricationRepository $ordreFabricationRepository): Response
    {
        return $this->render('ordre_fabrication/index.html.twig', [
            'ordre_fabrications' => $ordreFabricationRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="ordre_fabrication_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $ordreFabrication = new OrdreFabrication();
        $form = $this->createForm(OrdreFabricationType::class, $ordreFabrication);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ordreFabrication);
            $entityManager->flush();

            return $this->redirectToRoute('ordre_fabrication_index');
        }

        return $this->render('ordre_fabrication/new.html.twig', [
            'ordre_fabrication' => $ordreFabrication,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="ordre_fabrication_show", methods={"GET"})
     */
    public function show(OrdreFabrication $ordreFabrication): Response
    {
        return $this->render('ordre_fabrication/show.html.twig', [
            'ordre_fabrication' => $ordreFabrication,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="ordre_fabrication_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, OrdreFabrication $ordreFabrication): Response
    {
        $form = $this->createForm(OrdreFabricationType::class, $ordreFabrication);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            //preUpdate
            $this->getDoctrine()->getManager()->persist($ordreFabrication->getBijou());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ordre_fabrication_index', [
                'id' => $ordreFabrication->getId(),
            ]);
        }

        return $this->render('ordre_fabrication/edit.html.twig', [
            'ordre_fabrication' => $ordreFabrication,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="ordre_fabrication_delete", methods={"DELETE"})
     */
    public function delete(Request $request, OrdreFabrication $ordreFabrication): Response
    {
        if ($this->isCsrfTokenValid('delete'.$ordreFabrication->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ordreFabrication);
            $entityManager->flush();
        }

        return $this->redirectToRoute('ordre_fabrication_index');
    }
}
