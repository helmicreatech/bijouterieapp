<?php

namespace App\Repository;

use App\Entity\AchatAccessoire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AchatAccessoire|null find($id, $lockMode = null, $lockVersion = null)
 * @method AchatAccessoire|null findOneBy(array $criteria, array $orderBy = null)
 * @method AchatAccessoire[]    findAll()
 * @method AchatAccessoire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AchatAccessoireRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AchatAccessoire::class);
    }

    // /**
    //  * @return AchatAccessoire[] Returns an array of AchatAccessoire objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AchatAccessoire
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
