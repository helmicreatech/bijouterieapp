<?php

namespace App\Repository;

use App\Entity\CompositionOr;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CompositionOr|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompositionOr|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompositionOr[]    findAll()
 * @method CompositionOr[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompositionOrRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CompositionOr::class);
    }

    // /**
    //  * @return CompositionOr[] Returns an array of CompositionOr objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CompositionOr
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
