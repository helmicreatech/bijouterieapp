<?php

namespace App\Repository;

use App\Entity\Produit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Produit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Produit|null findOneBy(array $criteria, array $orderBy = null)
 * @method Produit[]    findAll()
 * @method Produit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProduitRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Produit::class);
    }

    // /**
    //  * @return Produit[] Returns an array of Produit objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Produit
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
   
    public function getPrixFabrication(Produit $produit)
    {
        $prixOr = 0;
        $prixPierres = 0;

        $compositionsOr = $produit->getGolds();
        foreach ($compositionsOr as $compo){
            $or = $compo->getGold();
            $prixOr += $compo->getQuantite()*$or->getPrixAchat();
        }

        $compositionsPierre = $produit->getPierres();
        foreach ($compositionsPierre as $compo){
            $pierre = $compo->getPierre();
			
			$ct1 = 0;
			if($pierre->getCarat()->getLibele() === '0005')
				$ct1 = 0.005;
			if($pierre->getCarat()->getLibele() == '001')
				$ct1 = 0.01;
			if($pierre->getCarat()->getLibele() == '002')
				$ct1 = 0.02;
			if($pierre->getCarat()->getLibele() == '003')
				$ct1 = 0.03;
			if($pierre->getCarat()->getLibele() == '004')
				$ct1 = 0.04;
			if($pierre->getCarat()->getLibele() === '005')
				$ct1 = 0.05;
			if($pierre->getCarat()->getLibele() == '006')
				$ct1 = 0.06;
			if($pierre->getCarat()->getLibele() == '007')
				$ct1 = 0.07;
			if($pierre->getCarat()->getLibele() == '008')
				$ct1 = 0.08;
			if($pierre->getCarat()->getLibele() == '009')
				$ct1 = 0.09;
			if($pierre->getCarat()->getLibele() == '010')
				$ct1 = 0.10;
			if($pierre->getCarat()->getLibele() == '011')
				$ct1 = 0.11;
			if($pierre->getCarat()->getLibele() == '012')
				$ct1 = 0.12;
			if($pierre->getCarat()->getLibele() == '013')
				$ct1 = 0.13;
			if($pierre->getCarat()->getLibele() == '014')
				$ct1 = 0.14;
			if($pierre->getCarat()->getLibele() == '015')
				$ct1 = 0.15;
			if($pierre->getCarat()->getLibele() == '016')
				$ct1 = 0.16;
			if($pierre->getCarat()->getLibele() == '017')
				$ct1 = 0.17;
			if($pierre->getCarat()->getLibele() == '018')
				$ct1 = 0.18;
			if($pierre->getCarat()->getLibele() == '019')
				$ct1 = 0.19;
			if($pierre->getCarat()->getLibele() == '020')
				$ct1 = 0.20;
			if($pierre->getCarat()->getLibele() == '021')
				$ct1 = 0.21;
			if($pierre->getCarat()->getLibele() == '022')
				$ct1 = 0.22;
			if($pierre->getCarat()->getLibele() == '023')
				$ct1 = 0.23;
			if($pierre->getCarat()->getLibele() == '024')
				$ct1 = 0.24;
			if($pierre->getCarat()->getLibele() == '025')
				$ct1 = 0.25;
			if($pierre->getCarat()->getLibele() == '030')
				$ct1 = 0.30;
			if($pierre->getCarat()->getLibele() == '040')
				$ct1 = 0.40;
			if($pierre->getCarat()->getLibele() == '050')
				$ct1 = 0.50;
			if($pierre->getCarat()->getLibele() == '070')
				$ct1 = 0.70;
		
            $prixPierres += $compo->getQuantite()*$pierre->getPrixAchat()/(1/$ct1);
			$prixPierres += $compo->getQuantite()*1.5;
        }

        return $prixOr + $prixPierres;
    }

    public function getQteFabricable(Produit $produit)
    {
        $qteFabricable = 999999;

        $compositionsOr = $produit->getGolds();
        foreach ($compositionsOr as $compo){
            $or = $compo->getGold();
            if($compo->getQuantite() > 0)
				$qte = intval($or->getQteEnStock()/$compo->getQuantite());
			else
				$qte = 0;
            if($qteFabricable > $qte) $qteFabricable = $qte;
        }

        $compositionsPierre = $produit->getPierres();
        foreach ($compositionsPierre as $compo){
            $pierre = $compo->getPierre();
            if($compo->getQuantite() > 0)
				$qte = intval($pierre->getQteEnStock()/$compo->getQuantite());
			else
				$qte = 0;
            if($qteFabricable > $qte) $qteFabricable = $qte;
        }

        return $qteFabricable;
    }

    public function getChiffreAffaireOr()
    {
        return $this->createQueryBuilder('p')
            ->select('SUM(p.qteEnStock*p.prixAchat) as chiffreAffaire')
            ->andWhere('p.categorie = :val')
            ->setParameter('val', 1)
            ->getQuery()
            ->getSingleScalarResult()
        ;   
    }

    public function getChiffreAffairePierre()
    {
        return $this->createQueryBuilder('p')
            ->select('SUM(p.qteEnStock*p.prixAchat) as chiffreAffaire')
            ->andWhere('p.categorie = :val')
            ->setParameter('val', 2)
            ->getQuery()
            ->getSingleScalarResult()
        ;   
    }

    public function getChiffreAffaireBijoux()
    {
        return $this->createQueryBuilder('p')
            ->select('SUM(p.qteEnStock*p.prixVente) as chiffreAffaire')
            ->andWhere('p.categorie = :val')
            ->setParameter('val', 3)
            ->getQuery()
            ->getSingleScalarResult()
        ;   
    }

    public function getChiffreAffaireAccessoires()
    {
        return $this->createQueryBuilder('p')
            ->select('SUM(p.qteEnStock*p.prixVente) as chiffreAffaire')
            ->andWhere('p.categorie = :val')
            ->setParameter('val', 4)
            ->getQuery()
            ->getSingleScalarResult()
        ;   
    }

    public function getChiffreAffaireBijouxEnCoursDeFabrication()
    {
        return $this->createQueryBuilder('p')
            ->select('SUM(p.qteEnCoursDeFabrication*p.prixVente) as chiffreAffaire')
            ->getQuery()
            ->getSingleScalarResult()
        ;   
    }

    public function findAllGolds(){
        return $this->createQueryBuilder('p')
            ->andWhere('p.categorie = :val')
            ->setParameter('val', 1)
            ->getQuery()
            ->getResult()
        ;
   }

   public function findAllPierres(){
        return $this->createQueryBuilder('p')
            ->andWhere('p.categorie = :val')
            ->setParameter('val', 2)
            ->getQuery()
            ->getResult()
        ;
   }

   public function findAllBijoux(){
        return $this->createQueryBuilder('p')
            ->andWhere('p.categorie = :val')
            ->setParameter('val', 3)
            ->getQuery()
            ->getResult()
        ;
   }

   public function findAllAccessoires(){
        return $this->createQueryBuilder('p')
            ->andWhere('p.categorie = :val')
            ->setParameter('val', 4)
            ->getQuery()
            ->getResult()
        ;
   }
}
