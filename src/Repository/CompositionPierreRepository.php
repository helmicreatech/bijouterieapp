<?php

namespace App\Repository;

use App\Entity\CompositionPierre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CompositionPierre|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompositionPierre|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompositionPierre[]    findAll()
 * @method CompositionPierre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompositionPierreRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CompositionPierre::class);
    }

    // /**
    //  * @return CompositionPierre[] Returns an array of CompositionPierre objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CompositionPierre
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
