<?php

namespace App\Repository;

use App\Entity\AchatPierre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AchatPierre|null find($id, $lockMode = null, $lockVersion = null)
 * @method AchatPierre|null findOneBy(array $criteria, array $orderBy = null)
 * @method AchatPierre[]    findAll()
 * @method AchatPierre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AchatPierreRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AchatPierre::class);
    }

    // /**
    //  * @return AchatPierre[] Returns an array of AchatPierre objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AchatPierre
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
