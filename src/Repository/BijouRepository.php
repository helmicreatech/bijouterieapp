<?php

namespace App\Repository;

use App\Entity\Bijou;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Bijou|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bijou|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bijou[]    findAll()
 * @method Bijou[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BijouRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Bijou::class);
    }

    // /**
    //  * @return Bijou[] Returns an array of Bijou objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Bijou
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
   
    public function getPrixFabrication(Bijou $bijou)
    {
        $prixOr = 0;
        $prixPierres = 0;

        $compositionsOr = $bijou->getGolds();
        foreach ($compositionsOr as $compo){
            $or = $compo->getGold();
            $prixOr += $compo->getQuantite()*$or->getPrixAchat();
        }

        $compositionsPierre = $bijou->getPierres();
        foreach ($compositionsPierre as $compo){
            $pierre = $compo->getPierre();
            $prixPierres += $compo->getQuantite()*$pierre->getPrixAchat();
        }

        return $prixOr + $prixPierres;
    }

    public function getQteFabricable(Bijou $bijou)
    {
        $qteFabricable = 999999;

        $compositionsOr = $bijou->getGolds();
        foreach ($compositionsOr as $compo){
            $or = $compo->getGold();
			if($compo->getQuantite() > 0)
				$qte = intval($or->getQteEnStock()/$compo->getQuantite());
			else
				$qte = 0;
            if($qteFabricable > $qte) $qteFabricable = $qte;
        }

        $compositionsPierre = $bijou->getPierres();
        foreach ($compositionsPierre as $compo){
            $pierre = $compo->getPierre();
			if($compo->getQuantite() > 0)
				$qte = intval($pierre->getQteEnStock()/$compo->getQuantite());
			else
				$qte = 0;
            if($qteFabricable > $qte) $qteFabricable = $qte;
        }

        return $qteFabricable;
    }
    public function getCHiffreAffaireBijoux()
    {
        return $this->createQueryBuilder('b')
            ->select('SUM(b.qteEnStock*b.prixVente) as chiffreAffaire')
            ->getQuery()
            ->getSingleScalarResult()
        ;   
    }

    public function getCHiffreAffaireBijouxEnCoursDeFabrication()
    {
        return $this->createQueryBuilder('b')
            ->select('SUM(b.qteEnCoursDeFabrication*b.prixVente) as chiffreAffaire')
            ->getQuery()
            ->getSingleScalarResult()
        ;   
    }
}
