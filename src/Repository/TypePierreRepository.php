<?php

namespace App\Repository;

use App\Entity\TypePierre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TypePierre|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypePierre|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypePierre[]    findAll()
 * @method TypePierre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypePierreRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TypePierre::class);
    }

    // /**
    //  * @return TypePierre[] Returns an array of TypePierre objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypePierre
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
