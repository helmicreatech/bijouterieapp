<?php

namespace App\Repository;

use App\Entity\Carat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Carat|null find($id, $lockMode = null, $lockVersion = null)
 * @method Carat|null findOneBy(array $criteria, array $orderBy = null)
 * @method Carat[]    findAll()
 * @method Carat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CaratRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Carat::class);
    }

    // /**
    //  * @return Carat[] Returns an array of Carat objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Carat
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
