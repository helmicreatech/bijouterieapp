<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompositionPierreRepository")
 */
class CompositionPierre
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Produit", inversedBy="compositionPierre")
     */
    private $pierre;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Produit", mappedBy="pierres")
     */
    private $bijoux;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quantite;

    public function __construct()
    {
        $this->bijoux = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->pierre->getReference();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPierre(): ?Produit
    {
        return $this->pierre;
    }

    public function setPierre(?Produit $pierre): self
    {
        $this->pierre = $pierre;

        return $this;
    }

    /**
     * @return Collection|Bijou[]
     */
    public function getBijoux(): Collection
    {
        return $this->bijoux;
    }

    public function addBijoux(Bijou $bijoux): self
    {
        if (!$this->bijoux->contains($bijoux)) {
            $this->bijoux[] = $bijoux;
            $bijoux->addPierre($this);
        }

        return $this;
    }

    public function removeBijoux(Bijou $bijoux): self
    {
        if ($this->bijoux->contains($bijoux)) {
            $this->bijoux->removeElement($bijoux);
            $bijoux->removePierre($this);
        }

        return $this;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(?int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }
}
