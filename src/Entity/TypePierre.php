<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TypePierreRepository")
 */
class TypePierre
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $libele;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Produit", mappedBy="typePierre")
     */
    private $pierres;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $unite;

    public function __construct()
    {
        $this->pierres = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->libele;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibele(): ?string
    {
        return $this->libele;
    }

    public function setLibele(?string $libele): self
    {
        $this->libele = $libele;

        return $this;
    }

    /**
     * @return Collection|Produit[]
     */
    public function getPierres(): Collection
    {
        return $this->pierres;
    }

    public function addPierre(Produit $pierre): self
    {
        if (!$this->pierres->contains($pierre)) {
            $this->pierres[] = $pierre;
            $pierre->setTypePierre($this);
        }

        return $this;
    }

    public function removePierre(Produit $pierre): self
    {
        if ($this->pierres->contains($pierre)) {
            $this->pierres->removeElement($pierre);
            // set the owning side to null (unless already changed)
            if ($pierre->getTypePierre() === $this) {
                $pierre->setTypePierre(null);
            }
        }

        return $this;
    }

    public function getUnite(): ?string
    {
        return $this->unite;
    }

    public function setUnite(?string $unite): self
    {
        $this->unite = $unite;

        return $this;
    }
}
