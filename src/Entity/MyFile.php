<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use AppBundle\Services\Utils;

/**
 * MyFile
 *
 * @ORM\HasLifecycleCallbacks
 */
abstract class MyFile
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255)
     */
    protected $filename;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    protected $url;

    /**
     * @var string
     *
     * @ORM\Column(name="real_filename", type="string", length=255, nullable=true)
     */
    protected $realFileName;

    /**
     * @var string
     *
     * @ORM\Column(name="extension", type="string", length=10, nullable=true)
     */
    protected $extension;

    /**
     * @var string
     *
     * @ORM\Column(name="absolute_url", type="string", length=255, nullable=true)
     */
    protected $absoluteUrl;
    
    /**
     * @var string
     *
     * @ORM\Column(name="date_creation", type="datetime", nullable=true)
     */
    protected $dateCreation;
    
    /**
     * @var string
     *
     * @ORM\Column(name="nombre_telechargements", type="integer", nullable=true)
     */
    protected $nombreTelechargements;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setDateCreation(new \DateTime());
    }
    
    

    /**
     * @Assert\File(maxSize="50000k")
     */
    protected $file;
    protected $uploadDir = 'uploads'; // (In web folder)
    protected $projectDir = null;
    protected $projectDirName = null;
    protected $thumbnailsPath = '120x90';
    protected $tempFileName;
    protected $tempThumbnailsFileName;
    protected $additionalData = null;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file name
     *
     * @param string $filename
     *
     * @return Image
     */
    public function setFileName($filename)
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * Get file name
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->filename;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Image
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set realFileName
     *
     * @param string $realFileName
     *
     * @return MyFile
     */
    public function setRealFileName($realFileName)
    {
        $this->realFileName = $realFileName;
        return $this;
    }

    /**
     * Get extension
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set extension
     *
     * @param string $extension
     *
     * @return MyFile
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
        return $this;
    }

    /**
     * Get realFileName
     *
     * @return string
     */
    public function getRealFileName()
    {
        return $this->realFileName;
    }

    public function getFile()
    {
        return $this->file;
    }

    // We modify the file setter, if there is an upload of another file
    public function setFile(UploadedFile $file)
    {
        $this->file = $file;

        // We check if we already have a file for this entity
        if (null !== $this->getUrl())
        {
            // Save the file name to delete it later
            $this->setFileName(null);
            $this->setRealFileName(null);
            $this->setUrl(null);
        }
    }

    /**
     * Get upload dir (e.g., /web/uploads/images)
     *  
     * @return $uploadDir
     */
    public function getUploadDir()
    {
        // We return the relative path to the image
        return $this->uploadDir;
    }

    /**
     * Set upload dir
     *
     * @param string $uploadDir (e.g., /web/uploads/images)
     * 
     */
    public function setUploadDir($uploadDir)
    {
        $this->uploadDir = $uploadDir;
    }

    /**
     * Get project dir (e.g., /var/www/symfony-project)
     *
     * @return $projectDir
     */
    protected function getProjectDir()
    {
        return $this->projectDir;
    }

    /**
     * Set project dir (e.g., /var/www/symfony-project)
     *
     * @param string $projectDir
     * 
     */
    protected function setProjectDir($projectDir)
    {
        $this->projectDir = $projectDir;
    }

    /**
     * Set project dir name
     *
     * @param string $projectDirName (e.g., symfony-project)
     * 
     */
    public function setProjectDirName($projectDirName)
    {
        $this->projectDirName = $this->getProjectDir();
    }

    /**
     * Get project dir name (e.g., symfony-project)
     *
     * @return $projectDirName
     */
    protected function getProjectDirName()
    {
        return $this->projectDirName;
    }

    /**
     * Set additional data
     *
     * @param string $additionalData
     * 
     */
    public function setAdditionalData($additionalData)
    {
        $this->additionalData = $additionalData;
        if ($this->additionalData->kernel)
        {
            $this->setProjectDir($this->additionalData->kernel->getProjectDir());
            $this->setProjectDirName(basename($this->getProjectDir()));
        }
    }

    /**
     * Get additional data
     *
     * @return $additionalData
     */
    protected function getAdditionalData()
    {
        return $this->additionalData;
    }

    /**
     * Set web absolute url (e.g., http://domain.com/symfony-project/web)
     *
     * @return string 
     * 
     */
    public function setAbsoluteUrl()
    {
        $this->absoluteUrl = $this->additionalData->request->getScheme() . '://' . $this->additionalData->request->getHttpHost() . $this->additionalData->request->getBasePath() . '/' . $this->getUrl();
    }
    
    public function setAbsoluteUrl2($url)
    {
        $this->absoluteUrl = $url;
    }
    
    /**
     * Get web absolute url (e.g., http://domain.com/symfony-project/web)
     *
     * @return string 
     * 
     */
    public function getAbsoluteUrl()
    {
        //$this->absoluteUrl = $this->additionalData->request->getScheme() . '://' . $this->additionalData->request->getHttpHost() . $this->additionalData->request->getBasePath() . '/' . $this->getUrl();
        return $this->absoluteUrl;
    }

    /**
     * --------------- Events -------------
     */

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null === $this->getFile())
        {
            return;
        }

        // Set file extension
        $this->setExtension($this->getFile()->guessExtension());
        $filename = sha1(uniqid(mt_rand(), true) . $this->getFile()->getClientOriginalName()) . '.' . $this->getExtension();
        // Set file name
        $this->setFileName($filename);
        // Set url
        $url = $this->getUploadDir() . '/' . $filename;
        $this->setUrl($url);
        $this->realFileName = $this->getFile()->getClientOriginalName();
        $this->setAbsoluteUrl();
        $this->setDateCreation(new \DateTime());
        $this->setNombreTelechargements(0);
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile())
        {
            return;
        }

        // If we had an old file, we delete it
        if (null !== $this->tempFileName)
        {
            $oldFile = $this->getUploadDir() . '/' . $this->tempFileName;
            if (file_exists($oldFile))
            {
                unlink($oldFile);
            }
        }

        if (!file_exists($this->getUploadDir()))
        {
            mkdir($this->getUploadDir(), 0775, true);
        }

        // We move the sent file to the directory of our choice
        $this->getFile()->move(
            $this->getUploadDir(), $this->getUrl()
        );
    }

    /**
     * @ORM\PreRemove()
     */
    public function preRemoveUpload()
    {
        // Temporarily save the file name
        $this->tempFileName = $this->getUrl();
        $this->tempThumbnailsFileName = $this->getUploadDir() . '/' . $this->thumbnailsPath . '/' . $this->getFileName();
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        // In PostRemove, we do not have access to the id, we use our saved name
        if (file_exists($this->tempFileName))
        {
            unlink($this->tempFileName);
        }

        // Remove thumbnails file
        // In PostRemove, we do not have access to the id, we use our saved name
        if (file_exists($this->tempThumbnailsFileName))
        {
            unlink($this->tempThumbnailsFileName);
        }
    }
    
    /**
     * Set dateCreation
     *
     * @param string $dateCreation
     *
     * @return Commande
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }
    
    /**
     * Set $nombreTelechargements
     *
     * @param int $nombreTelechargements
     *
     * @return Commande
     */
    public function setNombreTelechargements($nombreTelechargements)
    {
        $this->nombreTelechargements = $nombreTelechargements;

        return $this;
    }

    /**
     * Get $nombreTelechargements
     *
     * @return int
     */
    public function getNombreTelechargements()
    {
        return $this->nombreTelechargements;
    }

}
