<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrdreFabricationRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class OrdreFabrication
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quantite;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $etat;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Produit", inversedBy="ordreFabrications")
     */
    private $bijou;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateLivraison;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prix;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $avance;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Fabriquant", inversedBy="ordreFabrication")
     */
    private $fabriquant;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $utiliserStockInterne;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $etatPaiement;

    public function __construct()
    {
        
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(?int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(?string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getBijou(): ?Produit
    {
        return $this->bijou;
    }

    public function setBijou(?Produit $bijou): self
    {
        $this->bijou = $bijou;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        if($this->getEtat() == 'En cours de fabrication')
            $this->getBijou()->setQteEnCoursDeFabrication($this->getBijou()->getQteEnCoursDeFabrication()+$this->getQuantite());
        else
            $this->getBijou()->setQteEnStock($this->getBijou()->getQteEnStock()+$this->getQuantite());

        
        if($this->getUtiliserStockInterne() == 'Oui'){
            $compositionsOr = $this->getBijou()->getGolds();
            foreach ($compositionsOr as $compo){
                $or = $compo->getGold();
                $or->setQteEnStock($or->getQteEnStock()-($compo->getQuantite()*$this->getQuantite()));
            }

            $compositionsPierre = $this->getBijou()->getPierres();
            foreach ($compositionsPierre as $compo){
                $pierre = $compo->getPierre();
                $pierre->setQteEnStock($pierre->getQteEnStock()-($compo->getQuantite()*$this->getQuantite()));
            }
        }
    }

    /**
    * @ORM\PreUpdate
    */
    public function preUpdate($event)
    {
        
        $em = $event->getEntityManager();

        if ($event->hasChangedField('quantite')) {
            $old = $event->getOldValue('quantite');
            $new = $event->getNewValue('quantite');
        }else{
            $old = $new = $this->getQuantite();
        }

        if ($event->hasChangedField('etat')) {
            $oldEtat = $event->getOldValue('etat');
            $newEtat = $event->getNewValue('etat');
        }else{
            $oldEtat = $newEtat = $this->getEtat();
        }
        
        if($oldEtat == 'En cours de fabrication')
            $this->getBijou()->setQteEnCoursDeFabrication($this->getBijou()->getQteEnCoursDeFabrication()-$old);
        else
            $this->getBijou()->setQteEnStock($this->getBijou()->getQteEnStock()-$old);

        if($newEtat == 'En cours de fabrication')
            $this->getBijou()->setQteEnCoursDeFabrication($this->getBijou()->getQteEnCoursDeFabrication()+$new);
        else
            $this->getBijou()->setQteEnStock($this->getBijou()->getQteEnStock()+$new);

        if($this->getUtiliserStockInterne() == 'Oui'){
            $compositionsOr = $this->getBijou()->getGolds();
            foreach ($compositionsOr as $compo){
                $or = $compo->getGold();
                $or->setQteEnStock($or->getQteEnStock()+($compo->getQuantite()*($old-$new)));
            }

            $compositionsPierre = $this->getBijou()->getPierres();
            foreach ($compositionsPierre as $compo){
                $pierre = $compo->getPierre();
                $pierre->setQteEnStock($pierre->getQteEnStock()+($compo->getQuantite()*($old-$new)));
            }
        }

        $em = $event->getEntityManager();
        $uow = $em->getUnitOfWork();
        $meta = $em->getClassMetadata(get_class($this));
        $uow->recomputeSingleEntityChangeSet($meta, $this);
    }
 
    /**
     * @ORM\PreRemove
     */
    public function preRemove()
    {
        if($this->getEtat() == 'En cours de fabrication')
            $this->getBijou()->setQteEnCoursDeFabrication($this->getBijou()->getQteEnCoursDeFabrication()-$this->getQuantite());
        else
            $this->getBijou()->setQteEnStock($this->getBijou()->getQteEnStock()-$this->getQuantite());

        if($this->getUtiliserStockInterne() == 'Oui'){
            $compositionsOr = $this->getBijou()->getGolds();
            foreach ($compositionsOr as $compo){
                $or = $compo->getGold();
                $or->setQteEnStock($or->getQteEnStock()+($compo->getQuantite()*$this->getQuantite()));
            }

            $compositionsPierre = $this->getBijou()->getPierres();
            foreach ($compositionsPierre as $compo){
                $pierre = $compo->getPierre();
                $pierre->setQteEnStock($pierre->getQteEnStock()+($compo->getQuantite()*$this->getQuantite()));
            }
        }
    }

    public function getDateLivraison(): ?\DateTimeInterface
    {
        return $this->dateLivraison;
    }

    public function setDateLivraison(?\DateTimeInterface $dateLivraison): self
    {
        $this->dateLivraison = $dateLivraison;

        return $this;
    }

    public function getPrix(): ?string
    {
        return $this->prix;
    }

    public function setPrix(?string $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getAvance(): ?string
    {
        return $this->avance;
    }

    public function setAvance(?string $avance): self
    {
        $this->avance = $avance;

        return $this;
    }

     public function getFabriquant(): ?Fabriquant
    {
        return $this->fabriquant;
    }

    public function setFabriquant(?Fabriquant $fabriquant): self
    {
        $this->fabriquant = $fabriquant;

        return $this;
    }

    public function getUtiliserStockInterne(): ?string
    {
        return $this->utiliserStockInterne;
    }

    public function setUtiliserStockInterne(?string $utiliserStockInterne): self
    {
        $this->utiliserStockInterne = $utiliserStockInterne;

        return $this;
    }

    public function getEtatPaiement(): ?string
    {
        return $this->etatPaiement;
    }

    public function setEtatPaiement(?string $etatPaiement): self
    {
        $this->etatPaiement = $etatPaiement;

        return $this;
    }
}
