<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Entity\MyFile;

/**
 * ProduitPicto
 *
 * @ORM\Table(name="image_produit")
 * @ORM\Entity(repositoryClass="App\Repository\ImageProduitRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ImageProduit extends MyFile
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    protected $uploadDir = 'uploads/images'; // (In web folder)

    /**
     * Owner entity
     * 
     * @ORM\OneToOne(targetEntity="App\Entity\Produit", inversedBy="image")
     */
    protected $produit;
    
    public function __toString()
    {
        return strval( $this->getAbsoluteUrl() ); 
    }

    /**
     * Set produit
     *
     * @param Produit $produit
     *
     * @return ImageProduit
     */
    public function setProduit(\App\Entity\Produit $produit = null)
    {
        $this->produit = $produit;
        return $this;
    }

    /**
     * Get produit
     *
     * @return Produit
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        parent::preUpload();
        // .... Others treatment
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        parent::upload();
        // .... Others treatment
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        parent::removeUpload();
        // .... Others treatment
    }
}
