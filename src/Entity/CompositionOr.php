<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompositionOrRepository")
 */
class CompositionOr
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Produit", inversedBy="compositionOr")
     */
    private $gold;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quantite;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Produit", mappedBy="golds")
     */
    private $bijoux;

    public function __construct()
    {
        $this->bijoux = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getGold()->getReference();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGold(): ?Produit
    {
        return $this->gold;
    }

    public function setGold(?Produit $gold): self
    {
        $this->gold = $gold;

        return $this;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(?int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * @return Collection|Bijou[]
     */
    public function getBijoux(): Collection
    {
        return $this->bijoux;
    }

    public function addBijoux(Bijou $bijoux): self
    {
        if (!$this->bijoux->contains($bijoux)) {
            $this->bijoux[] = $bijoux;
            $bijoux->addGold($this);
        }

        return $this;
    }

    public function removeBijoux(Bijou $bijoux): self
    {
        if ($this->bijoux->contains($bijoux)) {
            $this->bijoux->removeElement($bijoux);
            $bijoux->removeGold($this);
        }

        return $this;
    }
}
