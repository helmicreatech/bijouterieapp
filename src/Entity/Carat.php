<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CaratRepository")
 */
class Carat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libele;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Produit", mappedBy="carat")
     */
    private $pierres;

    public function __construct()
    {
        $this->pierres = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->libele;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibele(): ?string
    {
        return $this->libele;
    }

    public function setLibele(string $libele): self
    {
        $this->libele = $libele;

        return $this;
    }

    /**
     * @return Collection|Produit[]
     */
    public function getPierres(): Collection
    {
        return $this->pierres;
    }

    public function addPierre(Produit $pierre): self
    {
        if (!$this->pierres->contains($pierre)) {
            $this->pierres[] = $pierre;
            $pierre->setCarat($this);
        }

        return $this;
    }

    public function removePierre(Produit $pierre): self
    {
        if ($this->pierres->contains($pierre)) {
            $this->pierres->removeElement($pierre);
            // set the owning side to null (unless already changed)
            if ($pierre->getCarat() === $this) {
                $pierre->setCarat(null);
            }
        }

        return $this;
    }
}
