<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AchatRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Achat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Produit", inversedBy="achats")
     */
    private $produit;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $prix;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quantite;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Fournisseur", inversedBy="achatOr")
     */
    private $fournisseur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduit(): ?Produit
    {
        return $this->produit;
    }

    public function setProduit(?Produit $produit): self
    {
        $this->produit = $produit;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(?float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(?int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $produit = $this->getProduit();
        $produit->setQteEnStock($produit->getQteEnStock()+$this->getQuantite());
    }

    /**
    * @ORM\PreUpdate
    */
    public function preUpdate($event)
    {
        if ($event->hasChangedField('quantite')) {
            $em = $event->getEntityManager();
            $old = $event->getOldValue('quantite');
            $new = $event->getNewValue('quantite');
            $this->getProduit()->setQteEnStock($this->getProduit()->getQteEnStock()+$new-$old);
            $em = $event->getEntityManager();
            $uow = $em->getUnitOfWork();
            $meta = $em->getClassMetadata(get_class($this));
            $uow->recomputeSingleEntityChangeSet($meta, $this);
        }
    }
 
    /**
     * @ORM\PreRemove
     */
    public function preRemove()
    {
        $produit = $this->getProduit();
        $produit->setQteEnStock($produit->getQteEnStock()-$this->getQuantite());
    }

    public function getFournisseur(): ?Fournisseur
    {
        return $this->fournisseur;
    }

    public function setFournisseur(?Fournisseur $fournisseur): self
    {
        $this->fournisseur = $fournisseur;

        return $this;
    }
}
