<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProduitRepository")
 */
class Produit
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\CompositionOr", inversedBy="bijoux" ,cascade={"persist","remove"})
     * @ORM\JoinTable(name="bijou_composition_or",
	 *      joinColumns={@ORM\JoinColumn(name="composition_or_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@ORM\JoinColumn(name="bijou_id", referencedColumnName="id")}
	 *      )
     */
    private $golds;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\CompositionPierre", inversedBy="bijoux", cascade={"persist","remove"})
     * @ORM\JoinTable(name="bijou_composition_pierre",
	 *      joinColumns={@ORM\JoinColumn(name="composition_pierre_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@ORM\JoinColumn(name="bijou_id", referencedColumnName="id")}
	 *      )
     */
    private $pierres;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CompositionOr", mappedBy="gold")
     */
    private $compositionOr;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $caratOr;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $couleur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $traitement;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CompositionPierre", mappedBy="pierre")
     */
    private $compositionPierre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Carat", inversedBy="pierres")
     */
    private $carat;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypePierre", inversedBy="pierres")
     */
    private $typePierre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categorie", inversedBy="produits")
     */
    private $categorie;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $reference;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $prixAchat;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $prixVente;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $remiseMax;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LigneDevis", mappedBy="bijou")
     */
    private $ligneDevis;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LigneFacture", mappedBy="bijou")
     */
    private $ligneFactures;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrdreFabrication", mappedBy="bijou")
     */
    private $ordreFabrications;

    /**
     * Image (image file)
     * Owner entity
     *
     * @ORM\OneToOne(targetEntity="\App\Entity\ImageProduit", mappedBy="produit", cascade={"persist","remove"}, orphanRemoval=true)
     */
    private $image;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $qteEnStock;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $qteEnCoursDeFabrication;

    public function __construct()
    {
        $this->golds = new ArrayCollection();
        $this->pierres = new ArrayCollection();
        $this->ordreFabrications = new ArrayCollection();
        $this->ligneDevis = new ArrayCollection();
        $this->ligneFactures = new ArrayCollection();
    }

    public function __toString()
    {
        if($this->reference)
            return $this->reference;
        else 
            return "--";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|CompositionOr[]
     */
    public function getGolds(): Collection
    {
        return $this->golds;
    }

    public function addGold(CompositionOr $gold): self
    {
        if (!$this->golds->contains($gold)) {
            $this->golds[] = $gold;
        }

        return $this;
    }

    public function removeGold(CompositionOr $gold): self
    {
        if ($this->golds->contains($gold)) {
            $this->golds->removeElement($gold);
        }

        return $this;
    }

    /**
     * @return Collection|CompositionPierre[]
     */
    public function getPierres(): Collection
    {
        return $this->pierres;
    }

    public function addPierre(CompositionPierre $pierre): self
    {
        if (!$this->pierres->contains($pierre)) {
            $this->pierres[] = $pierre;
        }

        return $this;
    }

    public function removePierre(CompositionPierre $pierre): self
    {
        if ($this->pierres->contains($pierre)) {
            $this->pierres->removeElement($pierre);
        }

        return $this;
    }

    /**
     * @return Collection|CompositionPierre[]
     */
    public function getCompositionPierre(): Collection
    {
        return $this->compositionPierre;
    }

    public function addCompositionPierre(CompositionPierre $compositionPierre): self
    {
        if (!$this->compositionPierre->contains($compositionPierre)) {
            $this->compositionPierre[] = $compositionPierre;
            $compositionPierre->setPierre($this);
        }

        return $this;
    }

    public function removeCompositionPierre(CompositionPierre $compositionPierre): self
    {
        if ($this->compositionPierre->contains($compositionPierre)) {
            $this->compositionPierre->removeElement($compositionPierre);
            // set the owning side to null (unless already changed)
            if ($compositionPierre->getPierre() === $this) {
                $compositionPierre->setPierre(null);
            }
        }

        return $this;
    }

    public function getCarat(): ?Carat
    {
        return $this->carat;
    }

    public function setCarat(?Carat $carat): self
    {
        $this->carat = $carat;

        return $this;
    }

    public function getTypePierre(): ?TypePierre
    {
        return $this->typePierre;
    }

    public function setTypePierre(?TypePierre $typePierre): self
    {
        $this->typePierre = $typePierre;

        return $this;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * @return Collection|CompositionOr[]
     */
    public function getCompositionOr(): Collection
    {
        return $this->compositionOr;
    }

    public function addCompositionOr(CompositionOr $compositionOr): self
    {
        if (!$this->compositionOr->contains($compositionOr)) {
            $this->compositionOr[] = $compositionOr;
            $compositionOr->setGold($this);
        }

        return $this;
    }

    public function removeCompositionOr(CompositionOr $compositionOr): self
    {
        if ($this->compositionOr->contains($compositionOr)) {
            $this->compositionOr->removeElement($compositionOr);
            // set the owning side to null (unless already changed)
            if ($compositionOr->getGold() === $this) {
                $compositionOr->setGold(null);
            }
        }

        return $this;
    }

    public function getCouleur(): ?string
    {
        return $this->couleur;
    }

    public function setCouleur(?string $couleur): self
    {
        $this->couleur = $couleur;

        return $this;
    }

    public function getTraitement(): ?string
    {
        return $this->traitement;
    }

    public function setTraitement(?string $traitement): self
    {
        $this->traitement = $traitement;

        return $this;
    }

    public function getCaratOr(): ?int
    {
        return $this->caratOr;
    }

    public function setCaratOr(?int $caratOr): self
    {
        $this->caratOr = $caratOr;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(?string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getPrixAchat(): ?float
    {
        return $this->prixAchat;
    }

    public function setPrixAchat(?float $prixAchat): self
    {
        $this->prixAchat = $prixAchat;

        return $this;
    }

    public function getPrixVente(): ?float
    {
        return $this->prixVente;
    }

    public function setPrixVente(?float $prixVente): self
    {
        $this->prixVente = $prixVente;

        return $this;
    }

    public function getRemiseMax(): ?int
    {
        return $this->remiseMax;
    }

    public function setRemiseMax(?int $remiseMax): self
    {
        $this->remiseMax = $remiseMax;

        return $this;
    }

    /**
     * @return Collection|LigneDevis[]
     */
    public function getLigneDevis(): Collection
    {
        return $this->ligneDevis;
    }

    public function addLigneDevis(LigneDevis $ligneDevis): self
    {
        if (!$this->ligneDevis->contains($ligneDevis)) {
            $this->ligneDevis[] = $ligneDevis;
            $ligneDevis->setBijou($this);
        }

        return $this;
    }

    public function removeLigneDevis(LigneDevis $ligneDevis): self
    {
        if ($this->ligneDevis->contains($ligneDevis)) {
            $this->ligneDevis->removeElement($ligneDevis);
            // set the owning side to null (unless already changed)
            if ($ligneDevis->getBijou() === $this) {
                $ligneDevis->setBijou(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|LigneFacture[]
     */
    public function getLigneFactures(): Collection
    {
        return $this->ligneFactures;
    }

    public function addLigneFacture(LigneFacture $ligneFacture): self
    {
        if (!$this->ligneFactures->contains($ligneFacture)) {
            $this->ligneFactures[] = $ligneFacture;
            $ligneFacture->setBijou($this);
        }

        return $this;
    }

    public function removeLigneFacture(LigneFacture $ligneFacture): self
    {
        if ($this->ligneFactures->contains($ligneFacture)) {
            $this->ligneFactures->removeElement($ligneFacture);
            // set the owning side to null (unless already changed)
            if ($ligneFacture->getBijou() === $this) {
                $ligneFacture->setBijou(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|OrdreFabrication[]
     */
    public function getOrdreFabrications(): Collection
    {
        return $this->ordreFabrications;
    }

    public function addOrdreFabrication(OrdreFabrication $ordreFabrication): self
    {
        if (!$this->ordreFabrications->contains($ordreFabrication)) {
            $this->ordreFabrications[] = $ordreFabrication;
            $ordreFabrication->setBijou($this);
        }

        return $this;
    }

    public function removeOrdreFabrication(OrdreFabrication $ordreFabrication): self
    {
        if ($this->ordreFabrications->contains($ordreFabrication)) {
            $this->ordreFabrications->removeElement($ordreFabrication);
            // set the owning side to null (unless already changed)
            if ($ordreFabrication->getBijou() === $this) {
                $ordreFabrication->setBijou(null);
            }
        }

        return $this;
    }

    /**
     * Set picto
     *
     * @param \App\Entity\ImageProduit $image
     *
     * @return Bijou
     */
    public function setImage(\App\Entity\ImageProduit $image = null)
    {
        if ($image)
            $image->setProduit($this);

        $this->image = $image;

        return $this;
    }

    /**
     * Get picto
     *
     * @return \App\Entity\ImageProduit
     */
    public function getImage()
    {
        return $this->image;
    }

    public function getQteEnStock(): ?float
    {
        return $this->qteEnStock;
    }

    public function setQteEnStock(?float $qteEnStock): self
    {
        $this->qteEnStock = $qteEnStock;

        return $this;
    }

    public function getQteEnCoursDeFabrication(): ?float
    {
        return $this->qteEnCoursDeFabrication;
    }

    public function setQteEnCoursDeFabrication(?float $qteEnCoursDeFabrication): self
    {
        $this->qteEnCoursDeFabrication = $qteEnCoursDeFabrication;

        return $this;
    }
}
