<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190624160754 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ligne_devis ADD CONSTRAINT FK_888B2F1B9E2EF1B5 FOREIGN KEY (bijou_id) REFERENCES produit (id)');
        $this->addSql('ALTER TABLE ligne_facture ADD CONSTRAINT FK_611F5A299E2EF1B5 FOREIGN KEY (bijou_id) REFERENCES produit (id)');
        $this->addSql('ALTER TABLE ordre_fabrication ADD CONSTRAINT FK_7FB222D29E2EF1B5 FOREIGN KEY (bijou_id) REFERENCES produit (id)');
        $this->addSql('ALTER TABLE produit ADD designation VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ligne_devis DROP FOREIGN KEY FK_888B2F1B9E2EF1B5');
        $this->addSql('ALTER TABLE ligne_facture DROP FOREIGN KEY FK_611F5A299E2EF1B5');
        $this->addSql('ALTER TABLE ordre_fabrication DROP FOREIGN KEY FK_7FB222D29E2EF1B5');
        $this->addSql('ALTER TABLE produit DROP designation');
    }
}
